/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ms_util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Administrador
 */
public class DB_Tabelas {

    public DB_Tabelas() {
             
      try (Connection conn = new Fabrica_Conexoes().getConnection();
              Statement stmt = conn.createStatement()){ 
 

         System.out.println("Criando as tabelas do Banco de Dados..."); 
         String sql = "CREATE TABLE IF NOT EXISTS REGISTRO("
                 + "ID IDENTITY PRIMARY KEY,"
                 + "NOME VARCHAR(255),"
                 + "CPF  VARCHAR(255))";    

  
         stmt.executeUpdate(sql);
         System.out.println("Tabela do Banco de Dados Criado..."); 

      } catch(SQLException se) { 
         //Handle errors for JDBC 
          System.out.println(se);
      } catch(Exception ex) { 
         //Handle errors for Class.forName 
          System.out.println(ex); 
      
      } //end try 
      System.out.println("ADEUS!");
    }
    
}
