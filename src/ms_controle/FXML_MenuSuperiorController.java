package ms_controle;

import java.net.URL;
import java.util.ResourceBundle;
import static javafx.application.Platform.exit;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Administrador
 */
public class FXML_MenuSuperiorController implements Initializable {

    
    @FXML 
    public void aoClicarAdicionar (){
        new MS_Controle().telaAdicionar();
    }
    
    @FXML
    public void aoClicarEditar(){
        new MS_Controle().telaEditar();  
    }
    
    @FXML
    public void aoClicarEstatistica (){
        new MS_Controle().telaEstatistica();
    }
    
    @FXML
    public void aoClicarDeletar (){
        
    }
    
    @FXML
    void menuSuperiorFechar (){
        exit();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
