
package ms_controle;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ms_util.DB_Tabelas;

/**
 *
 * @author Administrador
 */
public class MS_Controle extends Application {
    
        private Stage primeiroEstagio;
        private BorderPane minhaTela;
    
    
    @Override
    public void start(Stage stage) throws Exception {
        
        System.out.println(new DB_Tabelas());
        
        this.primeiroEstagio = stage;
        this.primeiroEstagio.setTitle("Ministério da Saúde - NEMS/MA");
        
            menuSuperior();
            menuPrincipal();
            
    }


    public void menuSuperior (){
          try {
            FXMLLoader root = new FXMLLoader();
            root.setLocation(getClass().getResource("FXML_MenuSuperior.fxml"));
            minhaTela = (BorderPane) root.load();
            
            Scene scene = new Scene(minhaTela);
            primeiroEstagio.setScene(scene);
            primeiroEstagio.show();
            
        } catch (IOException ex) {
            System.out.println(ex);
        }   
    }
    
      public void menuPrincipal (){
           try {
               FXMLLoader root = new FXMLLoader();
               root.setLocation(getClass().getResource("FXML_Ficha.fxml"));
               AnchorPane ancora = (AnchorPane) root.load();
               
               minhaTela.setCenter(ancora);
               FXML_FichaController controller = root.getController();
               
               
           } catch (IOException ex) {
               System.out.println(ex);
           }
           
           
       }
    
     public void telaAdicionar (){
        	
        try {
            
        Parent edita = FXMLLoader.load(getClass().getResource("FXML_Adicionar.fxml"));
        
        Stage novo = new Stage();
        novo.setTitle("Ministério da Saúde - NEMS/MA");
	novo.initModality(Modality.WINDOW_MODAL);
	novo.initOwner(primeiroEstagio);
	Scene nov = new Scene(edita);
	novo.setScene(nov);
        novo.show();

            
        } catch (IOException ex) {
            Logger.getLogger(MS_Controle.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
     
         public void telaEditar (){
        	
        try {
            
        Parent edita = FXMLLoader.load(getClass().getResource("FXML_Editar.fxml"));
        
        Stage novo = new Stage();
        novo.setTitle("Ministério da Saúde - NEMS/MA");
	novo.initModality(Modality.WINDOW_MODAL);
	novo.initOwner(primeiroEstagio);
	Scene nov = new Scene(edita);
	novo.setScene(nov);
        novo.show();

            
        } catch (IOException ex) {
            Logger.getLogger(MS_Controle.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
         
       public void telaEstatistica (){
        	
        try {
            
        Parent edita = FXMLLoader.load(getClass().getResource("FXML_Estatistica.fxml"));
        
        Stage novo = new Stage();
        novo.setTitle("Ministério da Saúde - NEMS/MA");
	novo.initModality(Modality.WINDOW_MODAL);
	novo.initOwner(primeiroEstagio);
	Scene nov = new Scene(edita);
	novo.setScene(nov);
        novo.show();

            
        } catch (IOException ex) {
            Logger.getLogger(MS_Controle.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
      
      
    public static void main(String[] args) {
        launch(args);
    }
    
}
